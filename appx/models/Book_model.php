<?php
  class Book_model extends CI_Model {
       
      public function __construct(){
          
        $this->load->database();
        
      }
      
      //API call - get a book record by isbn
      public function getbookbyisbn($isbn){  

           $this->db->select('teacher_id, teacehr_name, teacher_birthday, teacher_address,');

           $this->db->from('teacher_tbl');

           $this->db->where('teacher_id',$isbn);

           $query = $this->db->get();
           
           if($query->num_rows() == 1)
           {

               return $query->result_array();

           }
           else
           {

             return 0;

          }

      }

    //API call - get all books record
    public function getallbooks(){   

        $this->db->select('teacher_id, teacher_name, teacher_birthday, teacher_address');

        $this->db->from('teacher_tbl');

        $this->db->order_by("teacher_id", "desc"); 

        $query = $this->db->get();

        if($query->num_rows() > 0){

          return $query->result_array();

        }else{

          return 0;

        }

    }
   
   //API call - delete a book record
    public function delete($id){

       $this->db->where('teacher_id', $id);

       if($this->db->delete('teacher_tbl')){

          return true;

        }else{

          return false;

        }

   }
   
   //API call - add new book record
    public function add($data){

        if($this->db->insert('teacher_tbl', $data)){

           return true;

        }else{

           return false;

        }

    }
    
    //API call - update a book record
    public function update($id, $data){

       $this->db->where('teacher_id', $id);

       if($this->db->update('teacher_tbl', $data)){

          return true;

        }else{

          return false;

        }

    }

}