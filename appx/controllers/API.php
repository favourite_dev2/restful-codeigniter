<?php

require(APPPATH.'/libraries/REST_Controller.php');
 
class Api extends REST_Controller{
    
    public function __construct()
    {
        parent::__construct();

        $this->load->model('book_model');
    }
    //API - client sends isbn and on valid isbn book information is sent back
   /* function bookByIsbn_get(){		
    
        $isbn  = $this->get('isbn');
        
        if(!$isbn){

            $this->response("No ISBN specified", 400);

            exit;
        }

        $result = $this->book_model->getbookbyisbn( $isbn );

        if($result){

            $this->response($result, 200); 

            exit;
        } 
        else{

             $this->response("Invalid ISBN", 404);

            exit;
        }
    } */
    //API -  Fetch All books
    function books_post(){

        $result = $this->book_model->getallbooks();

        if($result){

            $this->response($result, 200); 

        } 

        else{

            $this->response("No record found", 404);

        }
    }
     
    //API - create a new book item in database.
    function addBook_post(){

         $teacher_name = $this->post('teacher_name');

         $teacher_birthday = $this->post('teacher_birthday');

         $teacher_address = $this->post('teacher_address');  
        
         if(!$teacher_name|| !$teacher_birthday || !$teacher_address){

                $this->response("Enter complete teacher information to save", 400);

         }else{

            $result = $this->book_model->add(array("teacher_name"=>$teacher_name,"teacher_birthday"=>$teacher_birthday, "teacher_address"=>$teacher_address,));

            if($result === 0){

                $this->response("teacher information could not be saved. Try again.", 404);

            }else{

                $this->response(true, 200);  
           
            }

        }

    }

    
    //API - update a book 
    function updateBook_post(){
         
         $teacher_name = $this->post('teacher_name');

         $teacher_birthday = $this->post('teacher_birthday');

         $teacher_address = $this->post('teacher_address');

         $teacher_id = $this->post('teacher_id');
        
         if(!$teacher_name || !$teacher_birthday|| !$teacher_address ||!$teacher_id){

                $this->response("Enter complete teacher information to save", 400);

         }else{
            $result = $this->book_model->update($teacher_id, array("teacher_name"=>$teacher_name, "teacher_birthday"=>$teacher_birthday, "teacher_address"=>$teacher_address));

            if($result === 0){

                $this->response("teacher information coild not be saved. Try again.", 404);

            }else{

                $this->response(true, 200);  

            }

        }

    }

    //API - delete a book 
    function deleteBook_post()
    {

        $teacher_id  = $this->post('teacher_id');

        if(!$teacher_id){

            $this->response("Parameter missing", 404);

        }
         
        if($this->book_model->delete($teacher_id))
        {

            $this->response(true, 200);

        } 
        else
        {

            $this->response("Failed", 400);

        }

    }


}